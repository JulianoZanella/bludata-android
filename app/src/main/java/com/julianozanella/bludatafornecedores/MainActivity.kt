package com.julianozanella.bludatafornecedores

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.julianozanella.bludatafornecedores.dto.Filter
import com.julianozanella.bludatafornecedores.view.EmpresasFragment
import com.julianozanella.bludatafornecedores.view.FornecedoresFragment
import com.julianozanella.bludatafornecedores.view.SearchDialogActivity
import com.julianozanella.bludatafornecedores.viewModel.FornecedorViewModel

class MainActivity : AppCompatActivity() {

    private val empresasFragment = EmpresasFragment()
    private val fornecedoresFragment = FornecedoresFragment()
    private var showSearch = false
    private var showSearchClear = false
    private lateinit var fornecedorViewModel: FornecedorViewModel

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        invalidateOptionsMenu()
        when (item.itemId) {
            R.id.navigation_enterprises -> {
                showSearch = false
                replaceFragment(empresasFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_providers -> {
                showSearch = true
                replaceFragment(fornecedoresFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        fornecedorViewModel = ViewModelProviders.of(this)[FornecedorViewModel::class.java]
        replaceFragment(empresasFragment)
    }

    private fun replaceFragment(fragment: Fragment) {
        val tag = fragment.javaClass.simpleName
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.fl_fragment_container,
                fragment,
                tag
            )
            .commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        val searchMenu = menu?.findItem(R.id.action_search)
        searchMenu?.isVisible = showSearch
        val clearMenu = menu?.findItem(R.id.action_clear)
        clearMenu?.isVisible = showSearchClear
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> search()
            R.id.action_clear -> clearSearch()
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun search(): Boolean {
        startActivityForResult(Intent(this, SearchDialogActivity::class.java), REQUEST_SEARCH)
        return true
    }

    private fun clearSearch(): Boolean {
        invalidateOptionsMenu()
        fornecedorViewModel.search(Filter())
        showSearchClear = false
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_SEARCH) {
            invalidateOptionsMenu()
            val filter: Filter? = data?.getSerializableExtra(SearchDialogActivity.PARAM_FILTER) as Filter?
            if (filter != null) {
                fornecedorViewModel.search(filter)
                showSearchClear = filter.hasData()
            }

        }
    }

    companion object {
        private const val REQUEST_SEARCH = 141
    }
}
