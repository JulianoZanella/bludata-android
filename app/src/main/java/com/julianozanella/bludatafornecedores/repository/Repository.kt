package com.julianozanella.bludatafornecedores.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.julianozanella.bludatafornecedores.dto.CustomProblemDetails
import com.julianozanella.bludatafornecedores.dto.ResultInfo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

abstract class Repository<T, DTO> : IRepository<T, DTO> {

    fun <T> getLiveData(call: Call<T>): LiveData<ResultInfo<T>> {
        val data = MutableLiveData<ResultInfo<T>>()
        call.enqueue(object : Callback<T> {
            override fun onFailure(call: Call<T>, t: Throwable) {
                val error = CustomProblemDetails()
                error.title = "Erro de conexão"
                error.detail = t.message ?: "Erro desconhecido"
                error.status = ERROR_CODE
                data.value = ResultInfo(ERROR_CODE, null, error)
            }

            override fun onResponse(call: Call<T>, response: Response<T>) =
                if (response.isSuccessful) {
                    data.value = ResultInfo(response.code(), response.body())
                } else {
                    val gson = Gson()
                    val error: CustomProblemDetails =
                        gson.fromJson(response.errorBody()?.charStream(), CustomProblemDetails::class.java)
                    data.value = ResultInfo(response.code(), response.body(), error)
                }
        })
        return data
    }

    companion object {
        const val ERROR_CODE = -115
    }
}