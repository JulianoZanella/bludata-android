package com.julianozanella.bludatafornecedores.repository

import android.arch.lifecycle.LiveData
import com.julianozanella.bludatafornecedores.config.RetrofitConfig
import com.julianozanella.bludatafornecedores.dto.EmpresaDTO
import com.julianozanella.bludatafornecedores.dto.ResultInfo
import com.julianozanella.bludatafornecedores.model.Empresa
import com.julianozanella.bludatafornecedores.service.EmpresaService

class EmpresaRepository : Repository<Empresa, EmpresaDTO>() {

    private val service: EmpresaService = RetrofitConfig().getEmpresaService()

    override fun getAll(): LiveData<ResultInfo<List<Empresa>>> = getLiveData(service.getAll())

    override fun getById(id: Int): LiveData<ResultInfo<Empresa>> = getLiveData(service.getById(id.toString()))

    override fun insert(obj: EmpresaDTO) = getLiveData(service.insert(obj))

    override fun update(obj: Empresa) = getLiveData(service.update(obj.id.toString(), obj))

    override fun delete(obj: Empresa) = getLiveData(service.delete(obj.id.toString()))

    companion object {
        private var INSTANCE: EmpresaRepository? = null

        fun getInstance(): EmpresaRepository {
            return INSTANCE ?: EmpresaRepository().also { INSTANCE = it }
        }
    }
}