package com.julianozanella.bludatafornecedores.repository

import android.arch.lifecycle.LiveData
import com.julianozanella.bludatafornecedores.config.RetrofitConfig
import com.julianozanella.bludatafornecedores.dto.Filter
import com.julianozanella.bludatafornecedores.dto.FornecedorDTO
import com.julianozanella.bludatafornecedores.dto.ResultInfo
import com.julianozanella.bludatafornecedores.extensions.toUTCString
import com.julianozanella.bludatafornecedores.model.Fornecedor
import com.julianozanella.bludatafornecedores.service.FornecedorService

class FornecedorRepository : Repository<Fornecedor, FornecedorDTO>() {

    private val service: FornecedorService = RetrofitConfig().getFornecedorService()

    override fun getAll(): LiveData<ResultInfo<List<Fornecedor>>> = getLiveData(service.getAll())

    fun getAll(filter: Filter): LiveData<ResultInfo<List<Fornecedor>>> {
        return getLiveData(
            service.getAll(
                filter.name,
                filter.cpfOuCnpj,
                filter.registeredMin.toUTCString(),
                filter.registeredMax.toUTCString()
            )
        )
    }

    override fun getById(id: Int): LiveData<ResultInfo<Fornecedor>> = getLiveData(service.getById(id.toString()))

    override fun insert(obj: FornecedorDTO) = getLiveData(service.insert(obj))

    override fun update(obj: Fornecedor) = getLiveData(service.update(obj.id.toString(), obj))

    override fun delete(obj: Fornecedor) = getLiveData(service.delete(obj.id.toString()))

    companion object {
        private var INSTANCE: FornecedorRepository? = null

        fun getInstance(): FornecedorRepository {
            return INSTANCE ?: FornecedorRepository().also { INSTANCE = it }
        }
    }
}