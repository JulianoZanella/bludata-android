package com.julianozanella.bludatafornecedores.repository

import android.arch.lifecycle.LiveData
import com.julianozanella.bludatafornecedores.config.RetrofitConfig
import com.julianozanella.bludatafornecedores.dto.ResultInfo
import com.julianozanella.bludatafornecedores.dto.TelefoneDTO
import com.julianozanella.bludatafornecedores.model.Telefone

class TelefoneRepository : Repository<Telefone, TelefoneDTO>() {

    private val service = RetrofitConfig().getTelefoneService()

    override fun getAll(): LiveData<ResultInfo<List<Telefone>>> {
        return getLiveData(service.getAll())
    }

    fun getAll(fornecedorId: Int): LiveData<ResultInfo<List<Telefone>>> {
        return getLiveData(service.getAll(fornecedorId.toString()))
    }

    override fun getById(id: Int): LiveData<ResultInfo<Telefone>> {
        return getLiveData(service.getById(id.toString()))
    }

    override fun insert(obj: TelefoneDTO): LiveData<ResultInfo<Telefone>> {
        return getLiveData(service.insert(obj))
    }

    override fun update(obj: Telefone): LiveData<ResultInfo<Telefone>> {
        return getLiveData(service.update(obj.id.toString(), obj))
    }

    override fun delete(obj: Telefone): LiveData<ResultInfo<Telefone>> {
        return getLiveData(service.delete(obj.id.toString()))
    }

    companion object {
        private var INSTANCE: TelefoneRepository? = null

        fun getInstance(): TelefoneRepository {
            return INSTANCE ?: TelefoneRepository().also { INSTANCE = it }
        }
    }
}