package com.julianozanella.bludatafornecedores.repository

import android.arch.lifecycle.LiveData
import com.julianozanella.bludatafornecedores.dto.ResultInfo

interface IRepository<T, DTO> {

    fun getAll(): LiveData<ResultInfo<List<T>>>

    fun getById(id: Int): LiveData<ResultInfo<T>>

    fun insert(obj: DTO): LiveData<ResultInfo<T>>

    fun update(obj: T): LiveData<ResultInfo<T>>

    fun delete(obj: T): LiveData<ResultInfo<T>>
}