package com.julianozanella.bludatafornecedores.model

import com.julianozanella.bludatafornecedores.extensions.getAge
import com.julianozanella.bludatafornecedores.extensions.toUTC
import java.io.Serializable
import java.util.*

class Fornecedor() : Serializable {
    var id: Int = 0
    var nome: String = ""
    var cpFouCNPJ: String = ""
    var dataCadastro: Date = Date().toUTC()
    var rg: String? = null
    var dataNascimento: Date? = null
    var empresa: Empresa = Empresa()
    var empresaId: Int = 0
    var telefones: List<Telefone> = mutableListOf()

    constructor(nome: String, cpFouCNPJ: String, empresa: Empresa) : this() {
        this.nome = nome
        this.cpFouCNPJ = cpFouCNPJ
        this.empresa = empresa
        this.empresaId = empresa.id
    }

    constructor(nome: String, cpFouCNPJ: String, rg: String?, dataNascimento: Date?, empresa: Empresa) : this() {
        this.nome = nome
        this.cpFouCNPJ = cpFouCNPJ
        this.rg = rg
        this.dataNascimento = dataNascimento
        this.empresa = empresa
        this.empresaId = empresa.id
    }

    fun isFisicalPerson() = rg != null || dataNascimento != null

    fun isMajor(): Boolean {
        dataNascimento ?: return false
        return dataNascimento!!.getAge() > 18
    }
}