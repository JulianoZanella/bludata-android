package com.julianozanella.bludatafornecedores.model

import java.io.Serializable

class Empresa(
    var id: Int = 0,
    var uf: String = "SC",
    var nomeFantasia: String = "",
    var cnpj: String = "",
    var fornecedores: List<Fornecedor> = mutableListOf()
) : Serializable {

    fun isParanaense() = uf == "PR"
}