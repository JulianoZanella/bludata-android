package com.julianozanella.bludatafornecedores.model

import java.io.Serializable

class Telefone(var id: Int = 0, var numero: String = "", var fornecedorId: Int = 0) : Serializable