package com.julianozanella.bludatafornecedores.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.julianozanella.bludatafornecedores.R
import com.julianozanella.bludatafornecedores.model.Empresa
import com.julianozanella.bludatafornecedores.viewModel.EmpresaViewModel
import kotlinx.android.synthetic.main.activity_empresa_details.*

class EmpresaDetailsActivity : AppCompatActivity() {

    private lateinit var viewModel: EmpresaViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_empresa_details)
        title = getString(R.string.title_details)
        viewModel = ViewModelProviders.of(this)[EmpresaViewModel::class.java]
    }

    override fun onStart() {
        super.onStart()
        val id: Int? = intent.extras?.getInt(PARAM_ID)
        if (id != null) {
            viewModel.getById(id)?.observe(this, Observer {
                it?.body?.let { empresa ->
                    fillValues(empresa)
                }
            })
        }
    }

    private fun fillValues(empresa: Empresa) {
        tv_enterprise_details_name.text = empresa.nomeFantasia
        tv_enterprise_details_cnpj.text = empresa.cnpj
        tv_enterprise_details_uf.text = empresa.uf

        val fornecedoresFragment = FornecedoresFragment.newInstance(empresa)
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.fl_enterprise_details_providers,
                fornecedoresFragment,
                null
            )
            .commit()
    }


    companion object {

        private const val PARAM_ID = "id"

        fun intent(from: Context?, empresaId: Int): Intent {
            val intent = Intent(from, EmpresaDetailsActivity::class.java)
            intent.putExtra(PARAM_ID, empresaId)
            return intent
        }
    }
}
