package com.julianozanella.bludatafornecedores.view

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.Toast
import com.julianozanella.bludatafornecedores.R
import com.julianozanella.bludatafornecedores.dto.FornecedorDTO
import com.julianozanella.bludatafornecedores.extensions.*
import com.julianozanella.bludatafornecedores.model.Empresa
import com.julianozanella.bludatafornecedores.model.Fornecedor
import com.julianozanella.bludatafornecedores.viewModel.FornecedorViewModel
import com.julianozanella.bludatafornecedores.viewModel.MasterViewModel
import kotlinx.android.synthetic.main.content_fornecedor_edit.*
import java.util.*

class FornecedorEditActivity : FormActivity<Fornecedor, FornecedorDTO>(), DatePickerFragmentDialog.OnDateSetListener,
    EmpresasPickerDialog.OnSelectEnterprise {

    private var selectedEnterprise: Empresa? = null
    private var fornecedor: Fornecedor? = null
    private var birthdate: Date? = null
    private var isFisicalPerson = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = getString(R.string.title_provider)
        validate()

        val fornecedor: Fornecedor? = intent.extras?.getSerializable(PARAM_PROVIDER) as Fornecedor?
        if (fornecedor != null) {
            fillValues(fornecedor)
        }
        val empresa: Empresa? = intent.extras?.getSerializable(PARAM_ENTERPRISE) as Empresa?
        if (empresa != null) {
            selectedEnterprise = empresa
            tv_providers_edit_enterprise.text = empresa.nomeFantasia
        }

        rg_type_of_person.setOnCheckedChangeListener { _, checkedId ->
            setPerson(checkedId == R.id.rb_providers_fisical)
        }

        val datePicker = DatePickerFragmentDialog()
        tv_providers_edit_birthdate.setOnFocusAndClickListener {
            datePicker.show(supportFragmentManager, "birthdate")
        }

        val enterprisePicker = EmpresasPickerDialog()
        tv_providers_edit_enterprise.setOnFocusAndClickListener {
            enterprisePicker.show(supportFragmentManager, "enterprise")
        }
    }

    private fun validate() {
        et_providers_edit_name.required(this)
        et_providers_edit_cpf.validate(
            { et_providers_edit_cpf.text.toString().isValidCpf() },
            getString(R.string.message_invalid_cpf)
        )
        et_providers_edit_cnpj.validate(
            { et_providers_edit_cnpj.text.toString().isValidCnpj() },
            getString(R.string.message_invalid_cnpj)
        )
    }

    private fun fillValues(fornecedor: Fornecedor) {
        this.fornecedor = fornecedor
        (viewModel as FornecedorViewModel).getEmpresa(fornecedor.empresaId).observe(this, Observer {
            if (it?.body != null) {
                selectedEnterprise = it.body
                tv_providers_edit_enterprise.text = selectedEnterprise!!.nomeFantasia
            }
        })
        fornecedor.let {
            et_providers_edit_name.setText(it.nome)
            et_providers_edit_cpf.setText(it.cpFouCNPJ)
            setPerson(it.isFisicalPerson())
            if (it.isFisicalPerson()) {
                birthdate = it.dataNascimento
                et_providers_edit_rg.setText(it.rg)
                tv_providers_edit_birthdate.text = birthdate?.formatShort()
            } else {
                rb_providers_legal.isChecked = true
                isFisicalPerson = false
            }
        }
    }

    private fun setPerson(fisical: Boolean) {
        isFisicalPerson = fisical
        tv_providers_edit_cpf.visibility = if (fisical) View.VISIBLE else View.GONE
        et_providers_edit_cpf.visibility = if (fisical) View.VISIBLE else View.GONE
        tv_providers_edit_cnpj.visibility = if (!fisical) View.VISIBLE else View.GONE
        et_providers_edit_cnpj.visibility = if (!fisical) View.VISIBLE else View.GONE
        tv_providers_edit_rg.visibility = if (fisical) View.VISIBLE else View.GONE
        et_providers_edit_rg.visibility = if (fisical) View.VISIBLE else View.GONE
        tv_providers_edit_birth.visibility = if (fisical) View.VISIBLE else View.GONE
        tv_providers_edit_birthdate.visibility = if (fisical) View.VISIBLE else View.GONE
    }

    private fun verifyParanaenseAge(fornecedor: Fornecedor): Boolean {
        if (fornecedor.isFisicalPerson() && fornecedor.empresa.isParanaense()) {
            return fornecedor.isMajor()
        }
        return true
    }

    private fun showDialog(title: String, message: String, fornecedor: Fornecedor) {
        val dialog = AlertDialog.Builder(this)
        dialog.setMessage(message)
            .setTitle(title)
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                startActivity(TelefonesActivity.getIntent(this, fornecedor))
                finish()
            }
            .setNegativeButton(getString(R.string.no)) { _, _ ->
                finish()
            }
            .setCancelable(false)
            .show()
    }

    override fun getViewModel(): Class<out MasterViewModel<Fornecedor, FornecedorDTO>> =
        FornecedorViewModel::class.java

    override fun getLayoutResourceID(): Int = R.layout.content_fornecedor_edit

    override fun isSaveButton(): Boolean = fornecedor == null

    override fun mainAction() {
        val name = et_providers_edit_name.text.toString()
        val cpf = et_providers_edit_cpf.text.toString()
        val cnpj = et_providers_edit_cnpj.text.toString()
        val rgText = et_providers_edit_rg.text.toString()

        if (!name.isValid() || selectedEnterprise == null) {
            showFillAllMessage()
        } else {
            val newProvider: Fornecedor = fornecedor ?: Fornecedor()
            newProvider.apply {
                empresa = selectedEnterprise!!
                empresaId = selectedEnterprise!!.id
                nome = name
            }

            if (isFisicalPerson) {
                if (!cpf.isValidCpf() || !rgText.isValid() || birthdate == null) {
                    showFillAllMessage()
                    return
                } else {
                    newProvider.cpFouCNPJ = cpf
                    newProvider.rg = rgText
                    newProvider.dataNascimento = birthdate
                }
            } else {
                if (!cnpj.isValid()) {
                    showFillAllMessage()
                    return
                } else {
                    newProvider.cpFouCNPJ = cnpj
                }
            }

            if (!verifyParanaenseAge(newProvider)) {
                Toast.makeText(this, getString(R.string.message_child_paranaense), Toast.LENGTH_SHORT).show()
            } else {
                if (newProvider.id > 0) {
                    update(newProvider) {
                        showDialog(
                            getString(R.string.message_update_success),
                            getString(R.string.message_want_update_phones),
                            newProvider
                        )
                    }
                } else {
                    insert(FornecedorDTO(newProvider)) {
                        showDialog(
                            getString(R.string.message_insert_success),
                            getString(R.string.message_want_insert_phones),
                            it.body!!
                        )
                    }
                }
            }
        }
    }

    override fun onDateSet(date: Date, tag: String?) {
        tv_providers_edit_birthdate.text = date.formatShort()
        birthdate = date
    }

    override fun onSelect(empresa: Empresa) {
        tv_providers_edit_enterprise.text = empresa.nomeFantasia
        selectedEnterprise = empresa
    }

    companion object {

        const val PARAM_PROVIDER = "provider"
        private const val PARAM_ENTERPRISE = "enterprise"

        fun intent(from: Context?, fornecedor: Fornecedor?, empresa: Empresa? = null): Intent {
            val intent = Intent(from, FornecedorEditActivity::class.java)
            intent.putExtra(PARAM_PROVIDER, fornecedor)
            intent.putExtra(PARAM_ENTERPRISE, empresa)
            return intent
        }
    }
}
