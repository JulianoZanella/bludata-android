package com.julianozanella.bludatafornecedores.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.Toast
import com.julianozanella.bludatafornecedores.R
import com.julianozanella.bludatafornecedores.dto.TelefoneDTO
import com.julianozanella.bludatafornecedores.extensions.isValidPhone
import com.julianozanella.bludatafornecedores.extensions.showErrorAlert
import com.julianozanella.bludatafornecedores.extensions.validate
import com.julianozanella.bludatafornecedores.model.Telefone
import com.julianozanella.bludatafornecedores.viewModel.TelefoneViewModel
import kotlinx.android.synthetic.main.dialog_fragment_phone_edit.*
import kotlinx.android.synthetic.main.dialog_fragment_phone_edit.view.*
import kotlinx.android.synthetic.main.proxy_screen.view.*

class TelefoneEditDialog : DialogFragment() {

    private var proxyContainer: FrameLayout? = null
    private var mainButton: Button? = null
    private var cancelButton: Button? = null
    private lateinit var viewModel: TelefoneViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rView = inflater.inflate(R.layout.dialog_fragment_phone_edit, container, false)
        viewModel = ViewModelProviders.of(activity!!)[TelefoneViewModel::class.java]

        rView.et_phone_number.validate(
            { rView.et_phone_number.rawText.toString().isValidPhone() },
            getString(R.string.message_required)
        )

        val fornecedorId: Int = arguments?.getInt(PROVIDER_ID) as Int
        val telefone: Telefone? = arguments?.getSerializable(PHONE) as Telefone?
        if (telefone != null) {
            rView.et_phone_number.setText(telefone.numero)
            rView.btn_phone_save.text = getString(R.string.update)
        }
        mainButton = rView.btn_phone_save
        cancelButton = rView.btn_phone_cancel
        proxyContainer = rView.fl_proxy_container

        rView.btn_phone_cancel.setOnClickListener { dismiss() }
        rView.btn_phone_save.setOnClickListener { save(fornecedorId, telefone) }

        return rView
    }

    private fun save(fornecedorId: Int, telefone: Telefone?) {
        if (!et_phone_number.rawText.toString().isValidPhone()) {
            Toast.makeText(activity, getString(R.string.message_fill_all), Toast.LENGTH_SHORT).show()
        } else {
            backEndActionStart()
            val newPhone = Telefone()
            newPhone.fornecedorId = fornecedorId
            newPhone.numero = et_phone_number.text.toString()
            if (telefone != null) {
                newPhone.id = telefone.id
                viewModel.update(newPhone)?.observe(this, Observer {
                    if (it != null) {
                        if (it.isSuccessful()) {
                            viewModel.reload()
                            backEndActionDone(telefone)
                            dismiss()
                        } else {
                            backEndActionDone(telefone)
                            activity?.showErrorAlert(it.error!!)
                        }
                    }
                })
            } else {
                viewModel.insert(TelefoneDTO(newPhone))?.observe(this, Observer {
                    if (it != null) {
                        if (it.isSuccessful()) {
                            viewModel.reload()
                            backEndActionDone(telefone)
                            dismiss()
                        } else {
                            backEndActionDone(telefone)
                            activity?.showErrorAlert(it.error!!)
                        }
                    }
                })
            }
        }
    }

    private fun backEndActionStart() {
        proxyContainer?.visibility = View.VISIBLE
        mainButton?.isEnabled = false
        cancelButton?.isEnabled = false
        mainButton?.text = getString(R.string.sending_data)

    }

    private fun backEndActionDone(telefone: Telefone?) {
        proxyContainer?.visibility = View.GONE
        mainButton?.isEnabled = true
        cancelButton?.isEnabled = true
        mainButton?.text = if (telefone != null) getString(R.string.update) else getString(R.string.save)
    }


    companion object {

        private const val PROVIDER_ID = "provId"
        private const val PHONE = "phone"

        fun newInstance(fornecedorId: Int, telefone: Telefone? = null) =
            TelefoneEditDialog().apply {
                arguments = Bundle().apply {
                    putInt(PROVIDER_ID, fornecedorId)
                    putSerializable(PHONE, telefone)
                }
            }
    }
}
