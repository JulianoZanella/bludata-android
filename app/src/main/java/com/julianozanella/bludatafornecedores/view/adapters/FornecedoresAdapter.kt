package com.julianozanella.bludatafornecedores.view.adapters

import com.julianozanella.bludatafornecedores.model.Fornecedor

class FornecedoresAdapter : ListAdapter<Fornecedor>() {

    override fun setData(holder: ViewHolder, data: Fornecedor) {
        holder.setData(data.nome, data.cpFouCNPJ)
    }
}