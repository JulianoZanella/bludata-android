package com.julianozanella.bludatafornecedores.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.julianozanella.bludatafornecedores.R
import com.julianozanella.bludatafornecedores.model.Fornecedor

class TelefonesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_telefones)

        val fornecedor = intent.extras?.getSerializable(PROVIDER) as Fornecedor?
        title = getString(R.string.title_phones, fornecedor?.nome)

        val fragment = if (fornecedor == null) TelefonesFragment() else TelefonesFragment.newInstance(fornecedor)
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.fl_phones,
                fragment,
                "frag-tag"
            )
            .commit()
    }

    companion object {

        private const val PROVIDER = "provider"

        fun getIntent(context: Context, fornecedor: Fornecedor): Intent {
            val intent = Intent(context, TelefonesActivity::class.java)
            intent.putExtra(PROVIDER, fornecedor)
            return intent
        }
    }
}
