package com.julianozanella.bludatafornecedores.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.julianozanella.bludatafornecedores.extensions.toUF
import com.julianozanella.bludatafornecedores.model.enums.UF

class UFsAdapter(private val context: Context) : BaseAdapter() {

    private val items: List<UF> = UF.values().asList()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val textView = TextView(context)
        textView.text = items[position].name
        textView.textSize = 20F
        return textView
    }

    override fun getItem(position: Int): UF {
        return items[position]
    }

    override fun getItemId(position: Int): Long {
        return items[position].ordinal.toLong()
    }

    override fun getCount(): Int {
        return items.size
    }

    fun getPosition(uf: String): Int {
        val u: UF? = uf.toUF()
        return items.indexOf(u ?: UF.SC)
    }
}