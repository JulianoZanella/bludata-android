package com.julianozanella.bludatafornecedores.view.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.julianozanella.bludatafornecedores.R
import kotlinx.android.synthetic.main.item_enterprise_or_provider.view.*

abstract class ListAdapter<T>(private val showButtons: Boolean = true) :
    RecyclerView.Adapter<ListAdapter<T>.ViewHolder>() {

    var clickListener: ItemClickListener<T>? = null


    var items: List<T> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): ViewHolder {
        val layout = LayoutInflater
            .from(parent.context)
            .inflate(
                R.layout.item_enterprise_or_provider,
                parent,
                false
            )
        return ViewHolder(layout)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setData(holder, items[position])
    }

    abstract fun setData(holder: ViewHolder, data: T)

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val tvTitle: TextView = itemView.tv_item_title
        private val tvSubtitle: TextView = itemView.tv_item_subtitle
        private val btnEdit: ImageButton = itemView.btn_item_edit
        private val btnDelete: ImageButton = itemView.btn_item_delete

        init {
            itemView.setOnClickListener(this)
            if (!showButtons) {
                btnEdit.visibility = View.GONE
                btnDelete.visibility = View.GONE
            }
            btnEdit.setOnClickListener(this)
            btnDelete.setOnClickListener(this)
        }

        fun setData(title: String, subtitle: String) {
            tvTitle.text = title
            tvSubtitle.text = subtitle
        }

        override fun onClick(v: View?) {
            clickListener?.onClick(adapterPosition, v, items[adapterPosition])
        }
    }

    interface ItemClickListener<T> {
        fun onClick(position: Int, view: View?, value: T)
    }
}