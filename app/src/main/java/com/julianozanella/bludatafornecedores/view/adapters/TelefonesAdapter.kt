package com.julianozanella.bludatafornecedores.view.adapters

import com.julianozanella.bludatafornecedores.model.Telefone

class TelefonesAdapter : ListAdapter<Telefone>() {

    override fun setData(holder: ViewHolder, data: Telefone) {
        holder.setData(data.numero, "")
    }
}