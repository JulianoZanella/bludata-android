package com.julianozanella.bludatafornecedores.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.julianozanella.bludatafornecedores.R
import com.julianozanella.bludatafornecedores.model.Empresa
import com.julianozanella.bludatafornecedores.view.adapters.EmpresasAdapter
import com.julianozanella.bludatafornecedores.view.adapters.ListAdapter
import com.julianozanella.bludatafornecedores.viewModel.EmpresaViewModel
import kotlinx.android.synthetic.main.dialog_fragment_enterprises.view.*

class EmpresasPickerDialog : DialogFragment() {

    private lateinit var viewModel: EmpresaViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rView = inflater.inflate(R.layout.dialog_fragment_enterprises, container, false)
        val adapter = EmpresasAdapter(false)
        rView.rv_dialog_list.adapter = adapter
        rView.rv_dialog_list.layoutManager = LinearLayoutManager(activity)
        val divider = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
        rView.rv_dialog_list.addItemDecoration(divider)
        adapter.clickListener = object : ListAdapter.ItemClickListener<Empresa> {
            override fun onClick(position: Int, view: View?, value: Empresa) {
                select(value)
            }
        }
        viewModel = ViewModelProviders.of(this)[EmpresaViewModel::class.java]
        viewModel.getAll().observe(this, Observer {
            if (it?.body != null) {
                adapter.items = it.body
            }
        })
        return rView
    }

    private fun select(empresa: Empresa) {
        if (activity is OnSelectEnterprise) {
            (activity as OnSelectEnterprise).onSelect(empresa)
        }
        dismiss()
    }

    interface OnSelectEnterprise {
        fun onSelect(empresa: Empresa)
    }
}
