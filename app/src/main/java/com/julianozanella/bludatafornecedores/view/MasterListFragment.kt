package com.julianozanella.bludatafornecedores.view

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.julianozanella.bludatafornecedores.R
import com.julianozanella.bludatafornecedores.extensions.showErrorAlert
import com.julianozanella.bludatafornecedores.view.adapters.ListAdapter
import com.julianozanella.bludatafornecedores.viewModel.MasterViewModel
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.proxy_screen.*

abstract class MasterListFragment<T, DTO> : Fragment() {

    private var viewModel: MasterViewModel<T, DTO>? = null
    private var adapter: ListAdapter<T>? = null

    override fun onStart() {
        super.onStart()
        viewModel?.reload()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setAdapterAndViewModel()
        fab_add.setOnClickListener { addAction() }
    }

    abstract fun setAdapterAndViewModel()

    fun setAdapter(adapter: ListAdapter<T>) {
        this.adapter = adapter
        rv_list.layoutManager = LinearLayoutManager(activity)
        rv_list.adapter = adapter
        val divider = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
        val draw = activity!!.getDrawable(R.drawable.divider_line)
        if (draw != null) divider.setDrawable(draw)
        rv_list.addItemDecoration(divider)
        adapter.clickListener = object : ListAdapter.ItemClickListener<T> {
            override fun onClick(position: Int, view: View?, value: T) {
                when (view?.id) {
                    R.id.btn_item_edit -> edit(value)
                    R.id.btn_item_delete -> delete(value)
                    else -> showDetails(value)
                }
            }
        }
    }

    fun setViewModel(viewModel: MasterViewModel<T, DTO>, observeAll: Boolean = true) {
        fl_proxy_container.visibility = View.VISIBLE
        fab_add.isEnabled = false

        this.viewModel = viewModel
        if (observeAll) {
            viewModel.getAll()?.observe(this, Observer {
                if (it?.body != null) {
                    if (it.isSuccessful()) {
                        setItems(it.body)
                    } else {
                        activity?.showErrorAlert(it.error!!)
                    }
                }
            })
        }
    }

    fun setItems(items: List<T>) {
        adapter?.items = items
        fl_proxy_container.visibility = View.GONE
        fab_add.isEnabled = true
    }

    fun delete(obj: T) {
        val alert = AlertDialog.Builder(activity)
        alert.setMessage(getString(R.string.message_delete_confirm))
            .setTitle(R.string.delete)
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                fl_proxy_container.visibility = View.VISIBLE
                fab_add.isEnabled = false

                viewModel?.delete(obj)?.observe(this, Observer {
                    if (it != null) {
                        if (it.isSuccessful()) {
                            viewModel?.reload()
                        } else {
                            activity?.showErrorAlert(it.error!!)
                        }
                        fl_proxy_container.visibility = View.GONE
                        fab_add.isEnabled = true
                    }
                })
            }
            .setNegativeButton(getString(R.string.no)) { _, _ -> }
            .setCancelable(false)
            .show()
    }

    abstract fun addAction()

    abstract fun edit(obj: T)

    abstract fun showDetails(obj: T)

    final override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }
}
