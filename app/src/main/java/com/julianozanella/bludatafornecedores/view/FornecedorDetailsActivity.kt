package com.julianozanella.bludatafornecedores.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.julianozanella.bludatafornecedores.R
import com.julianozanella.bludatafornecedores.extensions.formatShort
import com.julianozanella.bludatafornecedores.extensions.formatShortWithTime
import com.julianozanella.bludatafornecedores.extensions.toLocal
import com.julianozanella.bludatafornecedores.model.Fornecedor
import com.julianozanella.bludatafornecedores.viewModel.FornecedorViewModel
import kotlinx.android.synthetic.main.activity_fornecedor_details.*

class FornecedorDetailsActivity : AppCompatActivity() {

    private lateinit var viewModel: FornecedorViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fornecedor_details)
        title = getString(R.string.title_details)
        val fornecedorId: Int = intent.extras?.getInt(PARAM_ID) ?: 0
        viewModel = ViewModelProviders.of(this)[FornecedorViewModel::class.java]

        viewModel.getById(fornecedorId)?.observe(this, Observer {
            if (it?.body != null) {
                fillValues(it.body)
            }
        })
    }

    private fun fillValues(fornecedor: Fornecedor) {
        fornecedor.let {
            tv_provider_name.text = it.nome
            tv_provider_cpf_or_cnpj.text = it.cpFouCNPJ
            tv_provider_register_date.text = it.dataCadastro.toLocal().formatShortWithTime()
            if (it.isFisicalPerson()) {
                tv_provider_cpf_title.text = getString(R.string.cpf)
                tv_provider_rg_title.visibility = View.VISIBLE
                tv_provider_rg.visibility = View.VISIBLE
                tv_provider_rg.text = it.rg
                tv_provider_birthdate_title.visibility = View.VISIBLE
                tv_provider_birthdate.visibility = View.VISIBLE
                tv_provider_birthdate.text = it.dataNascimento!!.formatShort()
            }
        }
        val fonesFragment = TelefonesFragment.newInstance(fornecedor)
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.fl_provider_details_phones,
                fonesFragment,
                null
            )
            .commit()
    }

    companion object {

        private const val PARAM_ID = "id"

        fun intent(from: Context?, fornecedorId: Int): Intent {
            val intent = Intent(from, FornecedorDetailsActivity::class.java)
            intent.putExtra(PARAM_ID, fornecedorId)
            return intent
        }
    }
}
