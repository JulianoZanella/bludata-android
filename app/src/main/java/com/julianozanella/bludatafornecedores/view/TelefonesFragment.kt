package com.julianozanella.bludatafornecedores.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.julianozanella.bludatafornecedores.dto.TelefoneDTO
import com.julianozanella.bludatafornecedores.model.Fornecedor
import com.julianozanella.bludatafornecedores.model.Telefone
import com.julianozanella.bludatafornecedores.view.adapters.TelefonesAdapter
import com.julianozanella.bludatafornecedores.viewModel.TelefoneViewModel

class TelefonesFragment : MasterListFragment<Telefone, TelefoneDTO>() {

    private var fornecedor: Fornecedor? = null

    override fun setAdapterAndViewModel() {
        setAdapter(TelefonesAdapter())
        val viewModel = ViewModelProviders.of(activity!!)[TelefoneViewModel::class.java]
        fornecedor = arguments?.getSerializable(PROVIDER) as Fornecedor?
        setViewModel(viewModel, fornecedor == null)
        if (fornecedor != null) {
            viewModel.getAll(fornecedor!!.id).observe(this, Observer {
                if (it?.body != null) {
                    setItems(it.body)
                }
            })
        }
    }

    override fun addAction() {
        TelefoneEditDialog.newInstance(fornecedor!!.id).show(activity!!.supportFragmentManager, "add-phone")
    }

    override fun edit(obj: Telefone) {
        TelefoneEditDialog.newInstance(obj.fornecedorId, obj).show(activity!!.supportFragmentManager, "update-phone")
    }

    override fun showDetails(obj: Telefone) {
    }

    companion object {

        private const val PROVIDER = "provider"

        fun newInstance(fornecedor: Fornecedor) =
            TelefonesFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(PROVIDER, fornecedor)
                }
            }
    }
}