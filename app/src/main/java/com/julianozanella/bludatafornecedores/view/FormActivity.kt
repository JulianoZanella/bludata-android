package com.julianozanella.bludatafornecedores.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.julianozanella.bludatafornecedores.R
import com.julianozanella.bludatafornecedores.dto.ResultInfo
import com.julianozanella.bludatafornecedores.extensions.showErrorAlert
import com.julianozanella.bludatafornecedores.viewModel.MasterViewModel
import kotlinx.android.synthetic.main.content_form.*
import kotlinx.android.synthetic.main.proxy_screen.*

abstract class FormActivity<T, DTO> : AppCompatActivity() {

    protected lateinit var viewModel: MasterViewModel<T, DTO>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)

        viewModel = ViewModelProviders.of(this)[getViewModel()]

        View.inflate(this, getLayoutResourceID(), fl_form)

        btn_main.setOnClickListener { mainAction() }
        btn_cancel.setOnClickListener { finish() }
    }

    override fun onStart() {
        super.onStart()
        updateMainButtonText(false)
    }

    private fun showProxy(status: Boolean) {
        fl_proxy_container.visibility =
            if (status)
                View.VISIBLE
            else
                View.GONE
    }

    private fun backEndActionStart() {
        blockFields(true)
        updateMainButtonText(true)
        showProxy(true)
    }

    private fun blockFields(status: Boolean) {
        btn_main.isEnabled = !status
        btn_cancel.isEnabled = !status
    }

    private fun backEndActionDone() {
        blockFields(false)
        updateMainButtonText(false)
        showProxy(false)
    }

    private fun updateMainButtonText(status: Boolean, saveButton: Boolean = isSaveButton()) {
        btn_main.text =
            when {
                status -> getString(R.string.sending_data)
                saveButton -> getString(R.string.save)
                else -> getString(R.string.update)
            }
    }

    protected fun insert(obj: DTO, function: (obj: ResultInfo<T>) -> Unit) {
        backEndActionStart()
        viewModel.insert(obj)?.observe(this, Observer {
            if (it != null) {
                if (it.isSuccessful()) {
                    backEndActionDone()
                    function(it)
                } else {
                    backEndActionDone()
                    showErrorAlert(it.error!!)
                }
            }
        })
    }

    protected fun update(obj: T, function: (obj: ResultInfo<T>) -> Unit) {
        backEndActionStart()
        viewModel.update(obj)?.observe(this, Observer {
            if (it != null) {
                if (it.isSuccessful()) {
                    backEndActionDone()
                    function(it)
                } else {
                    backEndActionDone()
                    showErrorAlert(it.error!!)
                }
            }
        })
    }

    protected fun showFillAllMessage() {
        Toast.makeText(this, getString(R.string.message_fill_all), Toast.LENGTH_SHORT).show()
    }

    abstract fun getViewModel(): Class<out MasterViewModel<T, DTO>>

    abstract fun getLayoutResourceID(): Int

    abstract fun isSaveButton(): Boolean

    abstract fun mainAction()
}
