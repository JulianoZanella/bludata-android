package com.julianozanella.bludatafornecedores.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import com.julianozanella.bludatafornecedores.R
import com.julianozanella.bludatafornecedores.dto.EmpresaDTO
import com.julianozanella.bludatafornecedores.extensions.*
import com.julianozanella.bludatafornecedores.model.Empresa
import com.julianozanella.bludatafornecedores.model.enums.UF
import com.julianozanella.bludatafornecedores.view.adapters.UFsAdapter
import com.julianozanella.bludatafornecedores.viewModel.EmpresaViewModel
import com.julianozanella.bludatafornecedores.viewModel.MasterViewModel
import kotlinx.android.synthetic.main.content_empresa_edit.*

class EmpresaEditActivity : FormActivity<Empresa, EmpresaDTO>() {

    var id: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = getString(R.string.title_enterprise)

        et_enterprise_name.required(this)
        et_enterprise_cnpj.validate(
            { et_enterprise_cnpj.text.toString().isValidCnpj() },
            getString(R.string.message_invalid_cnpj)
        )

        val ufsAdapter = UFsAdapter(this)
        sp_ufs.adapter = ufsAdapter
        val savedUF = getPreference(KEY.UF, String::class.java) as String?
        if (savedUF != null) {//seleciona a ultima uf usada
            sp_ufs.setSelection(ufsAdapter.getPosition(savedUF))
        }
        sp_ufs.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                savePreference(KEY.UF, ufsAdapter.getItem(position).name)
            }
        }

        val empresa: Empresa? = intent?.extras?.getSerializable(PARAM_ENTERPRISE) as Empresa?
        empresa?.let {
            et_enterprise_name.setText(it.nomeFantasia)
            et_enterprise_cnpj.setText(it.cnpj)
            sp_ufs.setSelection(ufsAdapter.getPosition(it.uf))
        }
        id = empresa?.id ?: 0
    }

    override fun getViewModel(): Class<out MasterViewModel<Empresa, EmpresaDTO>> =
        EmpresaViewModel::class.java

    override fun getLayoutResourceID(): Int = R.layout.content_empresa_edit

    override fun isSaveButton(): Boolean = id == 0

    override fun mainAction() {
        if (!et_enterprise_name.text.toString().isValid() || !et_enterprise_cnpj.text.toString().isValidCnpj()) {
            showFillAllMessage()
        } else {
            val uf: UF = sp_ufs.selectedItem as UF
            val empresa = Empresa(
                id,
                uf.name,
                et_enterprise_name.text.toString(),
                et_enterprise_cnpj.text.toString()
            )
            if (id == 0) {
                insert(EmpresaDTO(empresa)) { finish() }
            } else {
                update(empresa) { finish() }
            }
        }
    }

    companion object {

        const val PARAM_ENTERPRISE = "enterprise"

        fun intent(from: Context?, empresa: Empresa?): Intent {
            val intent = Intent(from, EmpresaEditActivity::class.java)
            intent.putExtra(PARAM_ENTERPRISE, empresa)
            return intent
        }
    }
}
