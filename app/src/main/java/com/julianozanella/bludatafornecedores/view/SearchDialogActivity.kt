package com.julianozanella.bludatafornecedores.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.julianozanella.bludatafornecedores.R
import com.julianozanella.bludatafornecedores.dto.Filter
import com.julianozanella.bludatafornecedores.extensions.formatShort
import com.julianozanella.bludatafornecedores.extensions.setOnFocusAndClickListener
import kotlinx.android.synthetic.main.dialog_fragment_search_providers.*
import java.util.*

class SearchDialogActivity : AppCompatActivity(), DatePickerFragmentDialog.OnDateSetListener {

    private var minDate: Date? = null
    private var maxDate: Date? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_fragment_search_providers)
        setFinishOnTouchOutside(false)
        setResult(Activity.RESULT_CANCELED)

        val datePicker = DatePickerFragmentDialog()
        tv_search_registered_min.setOnFocusAndClickListener {
            datePicker.show(supportFragmentManager, MIN)
        }
        tv_search_registered_max.setOnFocusAndClickListener {
            datePicker.show(supportFragmentManager, MAX)
        }

        btn_search_cancel.setOnClickListener { finish() }
        btn_search.setOnClickListener { search() }
    }

    private fun search() {
        val filter = Filter()
        if (et_search_name.text.toString().isNotBlank()) filter.name = et_search_name.text.toString()
        if (et_search_cpf.text.toString().isNotBlank()) filter.cpfOuCnpj = et_search_cpf.text.toString()
        filter.registeredMin = minDate
        filter.registeredMax = maxDate

        val intent = Intent()
        intent.putExtra(PARAM_FILTER, filter)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onDateSet(date: Date, tag: String?) {
        when (tag) {
            MIN -> {
                minDate = date
                tv_search_registered_min.text = date.formatShort()
            }
            MAX -> {
                maxDate = date
                tv_search_registered_max.text = date.formatShort()
            }
        }
    }

    companion object {
        private const val MIN = "min"
        private const val MAX = "max"
        const val PARAM_FILTER = "filter"
    }
}
