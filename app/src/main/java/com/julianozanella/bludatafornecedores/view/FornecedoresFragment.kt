package com.julianozanella.bludatafornecedores.view


import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.julianozanella.bludatafornecedores.dto.FornecedorDTO
import com.julianozanella.bludatafornecedores.model.Empresa
import com.julianozanella.bludatafornecedores.model.Fornecedor
import com.julianozanella.bludatafornecedores.view.adapters.FornecedoresAdapter
import com.julianozanella.bludatafornecedores.viewModel.FornecedorViewModel

class FornecedoresFragment : MasterListFragment<Fornecedor, FornecedorDTO>() {

    private var empresa: Empresa? = null

    override fun setAdapterAndViewModel() {
        setAdapter(FornecedoresAdapter())
        val viewModel = ViewModelProviders.of(activity!!)[FornecedorViewModel::class.java]
        empresa = arguments?.getSerializable(ENTERPRISE) as Empresa?
        setViewModel(viewModel, empresa == null)
        if (empresa != null) {
            setItems(empresa!!.fornecedores)
        }
    }

    override fun addAction() {
        startActivity(FornecedorEditActivity.intent(activity, null, empresa))
    }

    override fun edit(obj: Fornecedor) {
        startActivity(FornecedorEditActivity.intent(activity, obj))
    }

    override fun showDetails(obj: Fornecedor) {
        startActivity(FornecedorDetailsActivity.intent(activity, obj.id))
    }

    companion object {

        private const val ENTERPRISE = "enterprise"

        fun newInstance(empresa: Empresa) =
            FornecedoresFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ENTERPRISE, empresa)
                }
            }
    }
}
