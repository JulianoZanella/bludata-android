package com.julianozanella.bludatafornecedores.view.adapters

import com.julianozanella.bludatafornecedores.model.Empresa

class EmpresasAdapter(showButtons: Boolean = true) : ListAdapter<Empresa>(showButtons) {

    override fun setData(holder: ViewHolder, data: Empresa) {
        holder.setData(data.nomeFantasia, data.cnpj)
    }
}