package com.julianozanella.bludatafornecedores.view

import android.arch.lifecycle.ViewModelProviders
import com.julianozanella.bludatafornecedores.dto.EmpresaDTO
import com.julianozanella.bludatafornecedores.model.Empresa
import com.julianozanella.bludatafornecedores.view.adapters.EmpresasAdapter
import com.julianozanella.bludatafornecedores.viewModel.EmpresaViewModel
import kotlinx.android.synthetic.main.fragment_list.*

class EmpresasFragment : MasterListFragment<Empresa, EmpresaDTO>() {

    override fun setAdapterAndViewModel() {
        setAdapter(EmpresasAdapter())
        val viewModel = ViewModelProviders.of(this)[EmpresaViewModel::class.java]
        setViewModel(viewModel)
    }

    override fun addAction() {
        startActivity(EmpresaEditActivity.intent(activity, null))
    }

    override fun edit(obj: Empresa) {
        startActivity(EmpresaEditActivity.intent(activity, obj))
    }

    override fun showDetails(obj: Empresa) {
        startActivity(EmpresaDetailsActivity.intent(activity, obj.id))
    }
}
