package com.julianozanella.bludatafornecedores.config

import com.google.gson.GsonBuilder
import com.julianozanella.bludatafornecedores.service.EmpresaService
import com.julianozanella.bludatafornecedores.service.FornecedorService
import com.julianozanella.bludatafornecedores.service.TelefoneService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitConfig {

    private var baseRetrofit: Retrofit? = null

    private val gson = GsonBuilder().setDateFormat(UTC_PATTERN).create()

    private fun getRetrofit(): Retrofit {
        if (baseRetrofit == null) {
            baseRetrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        }
        return baseRetrofit!!
    }

    fun getEmpresaService(): EmpresaService = this.getRetrofit().create(EmpresaService::class.java)

    fun getFornecedorService(): FornecedorService = this.getRetrofit().create(FornecedorService::class.java)

    fun getTelefoneService(): TelefoneService = this.getRetrofit().create(TelefoneService::class.java)

    companion object {
        private const val BASE_URL = "https://testebludataapi.azurewebsites.net/api/"
        const val UTC_PATTERN = "yyyy-MM-dd'T'HH:mm:ss"
    }
}