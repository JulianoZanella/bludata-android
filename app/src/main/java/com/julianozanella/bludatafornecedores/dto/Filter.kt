package com.julianozanella.bludatafornecedores.dto

import java.io.Serializable
import java.util.*

class Filter(
    var name: String? = null,
    var cpfOuCnpj: String? = null,
    var registeredMin: Date? = null,
    var registeredMax: Date? = null
) : Serializable {

    fun hasData(): Boolean {
        return name != null || cpfOuCnpj != null || registeredMin != null || registeredMax != null
    }
}