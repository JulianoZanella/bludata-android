package com.julianozanella.bludatafornecedores.dto

import com.julianozanella.bludatafornecedores.extensions.toUF
import com.julianozanella.bludatafornecedores.model.Empresa
import com.julianozanella.bludatafornecedores.model.enums.UF
import java.io.Serializable

class EmpresaDTO(var nomeFantasia: String, var cnpj: String, var uf: UF) : Serializable {

    constructor(nomeFantasia: String, cnpj: String, uf: String) : this(nomeFantasia, cnpj, uf.toUF()!!)

    constructor(empresa: Empresa) : this(empresa.nomeFantasia, empresa.cnpj, empresa.uf.toUF()!!)
}