package com.julianozanella.bludatafornecedores.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CustomProblemDetails(
    @SerializedName(value = "title", alternate = ["Title"]) var title: String = "",
    @SerializedName(value = "status", alternate = ["Status"]) var status: Int = 0,
    @SerializedName(value = "instance", alternate = ["Instance"]) var instance: String = "",
    @SerializedName(value = "detail", alternate = ["Detail"]) var detail: String = "",
    @SerializedName(value = "type", alternate = ["Type"]) var type: String = "",
    @SerializedName(value = "errorsFields") var errorsFields: List<ErrorField> = mutableListOf()
) : Serializable

