package com.julianozanella.bludatafornecedores.dto

import com.julianozanella.bludatafornecedores.model.Telefone

class TelefoneDTO(var numero: String = "", var fornecedorId: Int = 0) {

    constructor(telefone: Telefone) : this(telefone.numero, telefone.fornecedorId)
}