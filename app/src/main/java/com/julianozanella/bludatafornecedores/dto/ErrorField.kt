package com.julianozanella.bludatafornecedores.dto

import java.io.Serializable

class ErrorField(var field: String = "", var errors: List<String> = mutableListOf()) : Serializable {

    override fun toString(): String {
        return "ErrorField(field='$field', errors=$errors)"
    }
}