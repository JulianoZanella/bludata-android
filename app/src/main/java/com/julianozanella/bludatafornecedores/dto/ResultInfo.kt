package com.julianozanella.bludatafornecedores.dto

class ResultInfo<T>(
    val code: Int,
    val body: T?,
    var error: CustomProblemDetails? = null
) {

    fun isSuccessful(): Boolean {
        return code in 200..300
    }
}