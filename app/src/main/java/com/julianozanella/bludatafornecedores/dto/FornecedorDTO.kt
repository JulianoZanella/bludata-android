package com.julianozanella.bludatafornecedores.dto

import com.julianozanella.bludatafornecedores.extensions.toUTC
import com.julianozanella.bludatafornecedores.model.Fornecedor
import java.util.*

class FornecedorDTO(
    var nome: String,
    var cPFouCNPJ: String,
    var empresaId: Int,
    var dataCadastro: Date = Date().toUTC(),
    var rg: String? = null,
    var dataNascimento: Date? = null
) {
    constructor(fornecedor: Fornecedor) : this(
        fornecedor.nome,
        fornecedor.cpFouCNPJ,
        fornecedor.empresaId,
        fornecedor.dataCadastro,
        fornecedor.rg,
        fornecedor.dataNascimento
    )
}