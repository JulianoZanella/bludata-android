package com.julianozanella.bludatafornecedores.viewModel

import android.app.Application
import com.julianozanella.bludatafornecedores.dto.EmpresaDTO
import com.julianozanella.bludatafornecedores.model.Empresa
import com.julianozanella.bludatafornecedores.repository.EmpresaRepository

class EmpresaViewModel(application: Application) : MasterViewModel<Empresa, EmpresaDTO>(application) {

    init {
        setRepository(EmpresaRepository.getInstance())
    }
}