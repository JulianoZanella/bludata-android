package com.julianozanella.bludatafornecedores.viewModel

import android.app.Application
import android.arch.lifecycle.Transformations
import com.julianozanella.bludatafornecedores.dto.TelefoneDTO
import com.julianozanella.bludatafornecedores.model.Telefone
import com.julianozanella.bludatafornecedores.repository.TelefoneRepository

class TelefoneViewModel(application: Application) : MasterViewModel<Telefone, TelefoneDTO>(application) {

    private val repository = TelefoneRepository.getInstance()

    init {
        setRepository(repository)
    }

    fun getAll(fornecedorId: Int) = Transformations.switchMap(retry) {
        repository.getAll(fornecedorId)
    }
}