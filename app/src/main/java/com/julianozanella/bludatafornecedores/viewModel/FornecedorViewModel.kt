package com.julianozanella.bludatafornecedores.viewModel

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import com.julianozanella.bludatafornecedores.dto.Filter
import com.julianozanella.bludatafornecedores.dto.FornecedorDTO
import com.julianozanella.bludatafornecedores.dto.ResultInfo
import com.julianozanella.bludatafornecedores.model.Fornecedor
import com.julianozanella.bludatafornecedores.repository.EmpresaRepository
import com.julianozanella.bludatafornecedores.repository.FornecedorRepository

class FornecedorViewModel(application: Application) : MasterViewModel<Fornecedor, FornecedorDTO>(application) {

    private val empresaRepository: EmpresaRepository

    private val mutableFilter: MutableLiveData<Filter> = MutableLiveData()

    init {
        setRepository(FornecedorRepository.getInstance())
        empresaRepository = EmpresaRepository.getInstance()
        mutableFilter.value = Filter()
    }

    fun getEmpresa(id: Int) = empresaRepository.getById(id)

    override fun getAll(): LiveData<ResultInfo<List<Fornecedor>>> {
        val repo = if (getRepository() is FornecedorRepository) (getRepository() as FornecedorRepository) else null
        if (repo != null) {
            return Transformations.switchMap(mutableFilter) {
                repo.getAll(it)
            }
        }
        return super.getAll()
    }

    fun search(filter: Filter) {
        mutableFilter.value = filter
    }

    override fun reload() {
        super.reload()
        mutableFilter.value = Filter()
    }
}