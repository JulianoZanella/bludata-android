package com.julianozanella.bludatafornecedores.viewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import com.julianozanella.bludatafornecedores.repository.IRepository

abstract class MasterViewModel<T, DTO>(application: Application) : AndroidViewModel(application) {

    private var repository: IRepository<T, DTO>? = null
    protected val retry: MutableLiveData<Boolean> = MutableLiveData()

    init {
        retry.value = true
    }

    fun setRepository(repository: IRepository<T, DTO>) {
        this.repository = repository
        retry.value = true
    }

    fun getRepository() = repository

    open fun getAll() = Transformations.switchMap(retry) {
        repository?.getAll()
    }

    fun getById(id: Int) =
        repository?.getById(id)

    fun insert(obj: DTO) =
        repository?.insert(obj)

    fun update(obj: T) =
        repository?.update(obj)

    fun delete(obj: T) =
        repository?.delete(obj)

    open fun reload() {
        retry.value = !retry.value!!
    }
}