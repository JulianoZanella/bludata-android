package com.julianozanella.bludatafornecedores.service

import com.julianozanella.bludatafornecedores.dto.FornecedorDTO
import com.julianozanella.bludatafornecedores.model.Fornecedor
import retrofit2.Call
import retrofit2.http.*

interface FornecedorService {

    @GET(FORNECEDOR)
    fun getAll(
        @Query("nome") nome: String? = null,
        @Query("cpfOuCnpj") cpfOuCnpj: String? = null,
        @Query("cadastroMin") cadastroMin: String? = null,
        @Query("cadastroMax") cadastroMax: String? = null
    ): Call<List<Fornecedor>>

    @GET(FORNECEDOR_ID)
    fun getById(@Path(ID) id: String): Call<Fornecedor>

    @POST(FORNECEDOR)
    fun insert(@Body fornecedorDTO: FornecedorDTO): Call<Fornecedor>

    @PUT(FORNECEDOR_ID)
    fun update(@Path(ID) id: String, @Body empresa: Fornecedor): Call<Fornecedor>

    @DELETE(FORNECEDOR_ID)
    fun delete(@Path(ID) id: String): Call<Fornecedor>

    companion object {
        private const val FORNECEDOR = "fornecedores"
        private const val ID = "id"
        private const val FORNECEDOR_ID = "$FORNECEDOR/{$ID}"
    }
}