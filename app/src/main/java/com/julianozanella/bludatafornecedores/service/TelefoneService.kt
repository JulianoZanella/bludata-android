package com.julianozanella.bludatafornecedores.service

import com.julianozanella.bludatafornecedores.dto.TelefoneDTO
import com.julianozanella.bludatafornecedores.model.Telefone
import retrofit2.Call
import retrofit2.http.*

interface TelefoneService {

    @GET(TELEFONE)
    fun getAll(@Query("fornecedorId") fornecedorId: String? = null): Call<List<Telefone>>

    @GET(TELEFONE_ID)
    fun getById(@Path(ID) id: String): Call<Telefone>

    @POST(TELEFONE)
    fun insert(@Body telefoneDTO: TelefoneDTO): Call<Telefone>

    @PUT(TELEFONE_ID)
    fun update(@Path(ID) id: String, @Body empresa: Telefone): Call<Telefone>

    @DELETE(TELEFONE_ID)
    fun delete(@Path(ID) id: String): Call<Telefone>

    companion object {
        private const val TELEFONE = "telefones"
        private const val ID = "id"
        private const val TELEFONE_ID = "$TELEFONE/{$ID}"
    }
}