package com.julianozanella.bludatafornecedores.service

import com.julianozanella.bludatafornecedores.dto.EmpresaDTO
import com.julianozanella.bludatafornecedores.model.Empresa
import retrofit2.Call
import retrofit2.http.*

interface EmpresaService {

    @GET(EMPRESA)
    fun getAll(): Call<List<Empresa>>

    @GET(EMPRESA_ID)
    fun getById(@Path(ID) id: String): Call<Empresa>

    @POST(EMPRESA)
    fun insert(@Body empresaDTO: EmpresaDTO): Call<Empresa>

    @PUT(EMPRESA_ID)
    fun update(@Path(ID) id: String, @Body empresa: Empresa): Call<Empresa>

    @DELETE(EMPRESA_ID)
    fun delete(@Path(ID) id: String): Call<Empresa>

    companion object{
       private const val EMPRESA = "empresas"
       private const val ID = "id"
       private const val EMPRESA_ID = "$EMPRESA/{$ID}"
    }
}