package com.julianozanella.bludatafornecedores.extensions

import android.annotation.SuppressLint
import com.julianozanella.bludatafornecedores.config.RetrofitConfig
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


fun Date.toUTC(): Date {
    val sdfLocal = DateFormat.getDateTimeInstance()

    val sdfUTC = DateFormat.getDateTimeInstance()
    sdfUTC.timeZone = TimeZone.getTimeZone("UTC")
    return sdfLocal.parse(sdfUTC.format(this))
}

fun Date.toLocal(): Date {
    val sdfDefault = DateFormat.getDateTimeInstance()

    val sdfLocal = DateFormat.getDateTimeInstance()
    sdfLocal.timeZone = TimeZone.getDefault()
    return sdfDefault.parse(sdfLocal.format(this))
}

fun Date.formatShort(): String {
    val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
    return dateFormat.format(this)
}

fun Date.formatShortWithTime(): String {
    val timeFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT)
    return timeFormat.format(this)
}

fun String.parseShort(): Date {
    val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
    return dateFormat.parse(this)
}

fun Date.getAge(): Int {
    val today = Calendar.getInstance()
    val birthdate = Calendar.getInstance()
    birthdate.time = this
    var age = today.get(Calendar.YEAR) - birthdate.get(Calendar.YEAR)
    today.add(Calendar.YEAR, -age)
    if (birthdate.get(Calendar.DAY_OF_MONTH) > today.get(Calendar.DAY_OF_MONTH)) {
        age--
    }
    return age
}

@SuppressLint("SimpleDateFormat")
fun Date?.toUTCString(): String? {
    this ?: return null
    val format = SimpleDateFormat(RetrofitConfig.UTC_PATTERN)
    return format.format(this.toUTC())
}