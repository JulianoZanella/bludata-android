package com.julianozanella.bludatafornecedores.extensions

import android.app.AlertDialog
import android.content.Context
import com.julianozanella.bludatafornecedores.R
import com.julianozanella.bludatafornecedores.dto.CustomProblemDetails

fun Context.savePreference(key: KEY, value: Any) {
    val preferences = this.getSharedPreferences(ARQUIVO, Context.MODE_PRIVATE)
    when (value) {
        is String -> preferences.edit().putString(key.name, value).apply()
        is Int -> preferences.edit().putInt(key.name, value).apply()
        is Float -> preferences.edit().putFloat(key.name, value).apply()
    }
}

fun Context.getPreference(key: KEY, type: Class<out Any>): Any? {
    val preferences = this.getSharedPreferences(ARQUIVO, Context.MODE_PRIVATE)
    return when (type.getConstructor().newInstance()) {
        is String -> preferences.getString(key.name, null)
        is Int -> preferences.getInt(key.name, -1)
        is Float -> preferences.getFloat(key.name, -1F)
        else -> null
    }
}

fun Context.showErrorAlert(error: CustomProblemDetails) {
    val alert = AlertDialog.Builder(this)
    var message = "${error.detail}\n"
    if (error.errorsFields.isNotEmpty()) {
        error.errorsFields.forEach {
            message += "${it.field}: \n"
            it.errors.forEach { err ->
                message += "\t$err\n"
            }
        }
    }
    alert.setMessage(message)
        .setTitle("${error.title}: ${error.status}")
        .setPositiveButton(getString(R.string.ok)) { _, _ -> }
        .setCancelable(false)
        .show()
}

private const val ARQUIVO = "bludataPreferences"

enum class KEY {
    UF
}