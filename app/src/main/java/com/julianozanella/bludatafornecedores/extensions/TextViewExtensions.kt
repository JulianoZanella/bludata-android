package com.julianozanella.bludatafornecedores.extensions

import android.widget.TextView

fun TextView.setOnFocusAndClickListener(function: () -> Unit) {
    this.setOnFocusChangeListener { _, hasFocus ->
        if (hasFocus) {
            function()
        }
    }
    this.setOnClickListener {
        if (this.hasFocus()) {
            function()
        }
    }
}