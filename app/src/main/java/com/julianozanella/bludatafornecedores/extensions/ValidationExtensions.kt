package com.julianozanella.bludatafornecedores.extensions

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import com.julianozanella.bludatafornecedores.R

private fun EditText.afterTextChanged(invokeValidation: (String) -> Unit) {

    this.addTextChangedListener(object : TextWatcher {

        override fun afterTextChanged(content: Editable?) {
            invokeValidation(content.toString())
        }

        override fun beforeTextChanged(
            content: CharSequence?,
            start: Int,
            count: Int,
            after: Int
        ) {
        }

        override fun onTextChanged(
            content: CharSequence?,
            start: Int,
            before: Int,
            count: Int
        ) {
        }
    })
}

fun EditText.validate(
    validator: (String) -> Boolean,
    message: String
) {

    this.afterTextChanged {
        this.error =
            if (validator(it))
                null
            else
                message
    }
}

fun EditText.required(context: Context) {
    this.validate({ this.text.toString().isValid() }, context.getString(R.string.message_required))
}

fun String.isValid(): Boolean {
    return this.isNotEmpty()
}

fun String.isValidCpf(): Boolean {
    var cpf = this
    cpf = cpf.replace(".", "").replace("/", "")
    if (cpf == "00000000000" || cpf == "11111111111"
        || cpf == "22222222222" || cpf == "33333333333"
        || cpf == "44444444444" || cpf == "55555555555"
        || cpf == "66666666666" || cpf == "77777777777"
        || cpf == "88888888888" || cpf == "99999999999"
        || cpf.length != 11
    ) {
        return false
    }
    try {
        var soma = 0
        val dig1: Int
        val dig2: Int
        var rest: Int
        var posicao = 0
        for (mult in 10 downTo 2) {
            soma += mult * Integer.parseInt(cpf[posicao].toString())
            posicao++

        }
        rest = soma % 11

        dig1 = if (11 - rest > 9) {
            0
        } else {
            11 - rest
        }

        posicao = 0
        soma = 0
        for (mult in 11 downTo 2) {
            soma += mult * Integer.parseInt(cpf[posicao].toString())
            posicao++
        }
        rest = soma % 11
        dig2 = if (11 - rest > 9) {
            0
        } else {
            11 - rest
        }
        return dig1.toString() == cpf[9].toString() && dig2.toString() == cpf[10].toString()
    } catch (e: Exception) {
        return false
    }
}

fun String.isValidCnpj(): Boolean {
    try {
        var soma = 0
        val dig1: Int
        val dig2: Int
        var resto: Int
        val cnpj = this.replace(".", "")
        var cnpj2 = cnpj.replace("-", "")
        cnpj2 = cnpj2.replace("/", "")

        if (cnpj2.length < 14) return false

        var posicao = 0

        var mult = 5
        while (posicao <= 11) {
            soma += mult * Integer.parseInt(cnpj2[posicao].toString())
            posicao++
            if (mult == 2) {
                mult = 10
            }
            mult--
        }
        resto = soma % 11
        dig1 = if (resto < 2) 0 else 11 - resto

        posicao = 0
        soma = 0
        mult = 6
        while (posicao <= 12) {
            soma += mult * Integer.parseInt(cnpj2[posicao].toString())
            posicao++
            if (mult == 2) {
                mult = 10
            }
            mult--
        }
        resto = soma % 11
        dig2 = if (resto < 2) 0 else 11 - resto

        return dig1.toString() == cnpj2[12].toString() && dig2.toString() == cnpj2[13].toString()

    } catch (e: Exception) {
        return false
    }
}

fun String.isValidPhone(): Boolean {
    return this.length == 11
}

fun String.isValid(minLength: Int, maxLength: Int): Boolean {
    return this.isNotEmpty() && this.length in minLength until (maxLength + 1)
}
