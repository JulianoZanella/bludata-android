package com.julianozanella.bludatafornecedores.extensions

import com.julianozanella.bludatafornecedores.model.enums.UF

fun String.toUF(): UF?{
    for (u in UF.values()){
        if(u.name == this){
            return u
        }
    }
    return null
}